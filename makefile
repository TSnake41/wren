CFLAGS := -O3 -s

AR ?= ar

CFLAGS += -Isrc/include -Isrc/optional -Isrc/vm
CFLAGS += -DWREN_OPT_META=1 -DWREN_OPT_RANDOM=1

SRC := src/vm/wren_compiler.c src/vm/wren_core.c src/vm/wren_debug.c \
	src/vm/wren_primitive.c src/vm/wren_utils.c src/vm/wren_value.c \
	src/vm/wren_vm.c src/optional/wren_opt_meta.c src/optional/wren_opt_random.c

OBJ := $(SRC:.c=.o)

all: lib/libwren.a

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)

lib/libwren.a: $(OBJ)
	$(AR) rcu $@ $^

clean:
	rm -rf lib/libwren.a $(OBJ)

.PHONY: libwren.a clean
